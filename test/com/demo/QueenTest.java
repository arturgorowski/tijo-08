package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QueenTest {

    private RulesOfGame queen;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForQueen() {

        queen = new Queen();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForQueen() {

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 10);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-10, -1);
        pointTo = new Point2d(10, -1);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(5, 5);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 1);
        pointTo = new Point2d(-1, -2);
        assertFalse(queen.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(queen.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(20, 20);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(-21, -21);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, 4);
        pointTo = new Point2d(-3, 2);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(2, -1);
        assertTrue(queen.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(1, -1);
        assertFalse(queen.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);
    }
}

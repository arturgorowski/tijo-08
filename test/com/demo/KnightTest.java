package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KnightTest {

    private RulesOfGame knight;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForKnight() {

        knight = new Knight();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForKnight() {

        // TODO: Prosze dokonczyc implementacje testow...

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(11, 12);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(9, 12);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(12, 11);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(12, 12);
        assertFalse(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(15, 6);
        assertFalse(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);
    }
}

package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Rook implements RulesOfGame {
    private int x;
    private int y;

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if (moveFrom.getY() == moveTo.getY() && moveFrom.getX() == moveTo.getX() ){
            return false;
        }

        if(moveTo.getX() ==  moveFrom.getY() ){
            return true;
        }

        if(moveTo.getY() ==  moveFrom.getY() ){
            return true;
        }

        return false;
    }
}

package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Knight implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if (moveFrom.getY() == moveTo.getY() && moveFrom.getX() == moveTo.getX() ){
            return false;
        }

        if((moveTo.getX() - moveFrom.getX() == 1 || moveTo.getX() - moveFrom.getX() == -1) && (moveTo.getY() - moveFrom.getY() == 2 || moveTo.getY() - moveFrom.getY() == -2)){
            return true;
        }

        if((moveTo.getX() - moveFrom.getX() == 2 || moveTo.getX() - moveFrom.getX() == -2) && (moveTo.getY() - moveFrom.getY() == 1 || moveTo.getY() - moveFrom.getY() == -1)){
            return true;
        }

        return false;
    }
}

package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Pawn implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if (moveFrom.getY() == moveTo.getY() && moveFrom.getX() == moveTo.getX()) {
            return false;
        }

        if (moveTo.getX() != moveFrom.getX()) {
            return false;
        }

        if (moveTo.getY() - moveFrom.getY() == 1) {
            return true;
        }

        return false;
    }
}

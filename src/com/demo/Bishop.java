package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Bishop implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if (moveFrom.getY() == moveTo.getY() && moveFrom.getX() == moveTo.getX()) {
            return false;
        }
        if (moveTo.getY() < 0 && moveTo.getX() < 0 && moveFrom.getY() < 0 && moveFrom.getX() < 0){
            if (moveTo.getX() - moveFrom.getX() == moveTo.getY() - moveFrom.getY()) {
                return true;
            }
        }
        else if (moveTo.getY() < 0) {
            if (-(moveTo.getX() - moveFrom.getX()) == moveTo.getY() - moveFrom.getY()) {
                return true;
            }
        } else {
            if (moveTo.getX() - moveFrom.getX() == moveTo.getY() - moveFrom.getY()) {
                return true;
            }
        }
        return false;
    }
}

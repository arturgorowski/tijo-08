package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class King implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        if (moveFrom.getY() == moveTo.getY() && moveFrom.getX() == moveTo.getX() ){
            return false;
        }

        if(Math.abs(moveTo.getX() - moveFrom.getX()) < 2 && Math.abs((moveTo.getY() - moveFrom.getY())) < 2) {
            return true;
        }
        return false;
    }
}
